object dmWorkRadio: TdmWorkRadio
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 192
  Top = 107
  Height = 407
  Width = 568
  object tbClientes: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'RazaoSocial'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Endereco'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'Bairro'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'Cidade'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'UF'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CEP'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'Telefone1'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Telefone2'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Fax'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Site'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'Email'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Cnpj_Cpf'
        DataType = ftString
        Size = 18
      end
      item
        Name = 'IE_RG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Responsavel'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Cpf_resp'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'RG_resp'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Email_resp'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Aniversario_resp'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Endereco_corresp'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'Bairro_corresp'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'Cidade_corresp'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'UF_corresp'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CEP_corresp'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'Situacao'
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'Clientes01'
        Fields = 'Codigo'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'Clientes02'
        Fields = 'Nome'
      end
      item
        Name = 'Clientes03'
        Fields = 'Cnpj_Cpf'
      end>
    StoreDefs = True
    TableName = 'CLIENTES'
    Left = 24
    Top = 80
    object tbClientesCodigo: TIBStringField
      FieldName = 'Codigo'
      Size = 4
    end
    object tbClientesNome: TIBStringField
      FieldName = 'Nome'
      Size = 40
    end
    object tbClientesRazaoSocial: TIBStringField
      FieldName = 'RazaoSocial'
      Size = 40
    end
    object tbClientesEndereco: TIBStringField
      FieldName = 'Endereco'
      Size = 35
    end
    object tbClientesBairro: TIBStringField
      FieldName = 'Bairro'
      Size = 25
    end
    object tbClientesCidade: TIBStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object tbClientesUF: TIBStringField
      FieldName = 'UF'
      Size = 2
    end
    object tbClientesCEP: TIBStringField
      FieldName = 'CEP'
      Size = 9
    end
    object tbClientesTelefone1: TIBStringField
      FieldName = 'Telefone1'
      Size = 15
    end
    object tbClientesTelefone2: TIBStringField
      FieldName = 'Telefone2'
      Size = 15
    end
    object tbClientesFax: TIBStringField
      FieldName = 'Fax'
      Size = 15
    end
    object tbClientesSite: TIBStringField
      FieldName = 'Site'
      Size = 35
    end
    object tbClientesEmail: TIBStringField
      FieldName = 'Email'
      Size = 30
    end
    object tbClientesCnpj_Cpf: TIBStringField
      FieldName = 'Cnpj_Cpf'
      Size = 18
    end
    object tbClientesIE_RG: TIBStringField
      FieldName = 'IE_RG'
    end
    object tbClientesResponsavel: TIBStringField
      FieldName = 'Responsavel'
      Size = 40
    end
    object tbClientesCpf_resp: TIBStringField
      FieldName = 'Cpf_resp'
      Size = 14
    end
    object tbClientesRG_resp: TIBStringField
      FieldName = 'RG_resp'
      Size = 15
    end
    object tbClientesEmail_resp: TIBStringField
      FieldName = 'Email_resp'
      Size = 40
    end
    object tbClientesAniversario_resp: TIBStringField
      FieldName = 'Aniversario_resp'
      Size = 6
    end
    object tbClientesEndereco_corresp: TIBStringField
      FieldName = 'Endereco_corresp'
      Size = 35
    end
    object tbClientesBairro_corresp: TIBStringField
      FieldName = 'Bairro_corresp'
      Size = 25
    end
    object tbClientesCidade_corresp: TIBStringField
      FieldName = 'Cidade_corresp'
      Size = 25
    end
    object tbClientesUF_corresp: TIBStringField
      FieldName = 'UF_corresp'
      Size = 2
    end
    object tbClientesCEP_corresp: TIBStringField
      FieldName = 'CEP_corresp'
      Size = 9
    end
    object tbClientesSituacao: TIBStringField
      FieldName = 'Situacao'
      Size = 1
    end
  end
  object dbWorkRadio: TIBDatabase
    Connected = True
    DatabaseName = 'work:F:\F\Delphi\GDB\WORKRADIO.GDB'
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey')
    LoginPrompt = False
    DefaultTransaction = ibtWorkRadio
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    AllowStreamedConnected = False
    Left = 24
    Top = 8
  end
  object ibtWorkRadio: TIBTransaction
    Active = False
    DefaultDatabase = dbWorkRadio
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    AutoStopAction = saCommit
    Left = 104
    Top = 8
  end
  object tbProdutos: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Codigo'
        Attributes = [faRequired]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'Descricao'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <
      item
        Name = 'Produtos01'
        Fields = 'Codigo'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'Produtos02'
        Fields = 'Descricao'
      end>
    StoreDefs = True
    TableName = 'PRODUTOS'
    Left = 408
    Top = 80
    object tbProdutosCodigo: TIBStringField
      FieldName = 'Codigo'
      Size = 3
    end
    object tbProdutosDescricao: TIBStringField
      FieldName = 'Descricao'
    end
  end
  object tbPublicitarios: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Endereco'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'Bairro'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'Cidade'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'UF'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CEP'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'Tele1'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Tele2'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Email'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'Cpf'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'RG'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Comissao1'
        DataType = ftFloat
      end
      item
        Name = 'Comissao2'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'Publicitarios01'
        Fields = 'Codigo'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'Publicitarios02'
        Fields = 'Nome'
        Options = [ixCaseInsensitive]
      end>
    StoreDefs = True
    TableName = 'PUBLICITARIOS'
    Left = 96
    Top = 80
    object tbPublicitariosCodigo: TIBStringField
      FieldName = 'Codigo'
      Size = 3
    end
    object tbPublicitariosNome: TIBStringField
      FieldName = 'Nome'
      Size = 40
    end
    object tbPublicitariosEndereco: TIBStringField
      FieldName = 'Endereco'
      Size = 35
    end
    object tbPublicitariosBairro: TIBStringField
      FieldName = 'Bairro'
      Size = 25
    end
    object tbPublicitariosCidade: TIBStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object tbPublicitariosUF: TIBStringField
      FieldName = 'UF'
      Size = 2
    end
    object tbPublicitariosCEP: TIBStringField
      FieldName = 'CEP'
      Size = 9
    end
    object tbPublicitariosTele1: TIBStringField
      FieldName = 'Tele1'
      Size = 15
    end
    object tbPublicitariosTele2: TIBStringField
      FieldName = 'Tele2'
      Size = 15
    end
    object tbPublicitariosEmail: TIBStringField
      FieldName = 'Email'
      Size = 35
    end
    object tbPublicitariosCpf: TIBStringField
      FieldName = 'Cpf'
      Size = 14
    end
    object tbPublicitariosRG: TIBStringField
      FieldName = 'RG'
      Size = 15
    end
    object tbPublicitariosComissao1: TFloatField
      FieldName = 'Comissao1'
    end
    object tbPublicitariosComissao2: TFloatField
      FieldName = 'Comissao2'
    end
  end
  object tbProgramas: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Codigo'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Programa'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Horario'
        DataType = ftTime
      end
      item
        Name = 'AudioAbertura'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Comerciais'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PosicaoBloco'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AudioFinal'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Domingo'
        DataType = ftSmallint
      end
      item
        Name = 'Segunda'
        DataType = ftSmallint
      end
      item
        Name = 'Terca'
        DataType = ftSmallint
      end
      item
        Name = 'Quarta'
        DataType = ftSmallint
      end
      item
        Name = 'Quinta'
        DataType = ftSmallint
      end
      item
        Name = 'Sexta'
        DataType = ftSmallint
      end
      item
        Name = 'Sabado'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'Programas01'
        Fields = 'Codigo'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'Programas02'
        Fields = 'Programa'
        Options = [ixCaseInsensitive]
      end>
    StoreDefs = True
    TableName = 'PROGRAMAS'
    Left = 240
    Top = 80
    object tbProgramasCodigo: TIBStringField
      FieldName = 'Codigo'
      Size = 2
    end
    object tbProgramasPrograma: TIBStringField
      FieldName = 'Programa'
      Size = 30
    end
    object tbProgramasHorario: TTimeField
      FieldName = 'Horario'
    end
    object tbProgramasAudioAbertura: TIBStringField
      FieldName = 'AudioAbertura'
      Size = 8
    end
    object tbProgramasComerciais: TIBStringField
      FieldName = 'Comerciais'
      Size = 1
    end
    object tbProgramasPosicaoBloco: TIBStringField
      FieldName = 'PosicaoBloco'
      Size = 1
    end
    object tbProgramasAudioFinal: TIBStringField
      FieldName = 'AudioFinal'
      Size = 8
    end
    object tbProgramasDomingo: TSmallintField
      FieldName = 'Domingo'
    end
    object tbProgramasSegunda: TSmallintField
      FieldName = 'Segunda'
    end
    object tbProgramasTerca: TSmallintField
      FieldName = 'Terca'
    end
    object tbProgramasQuarta: TSmallintField
      FieldName = 'Quarta'
    end
    object tbProgramasQuinta: TSmallintField
      FieldName = 'Quinta'
    end
    object tbProgramasSexta: TSmallintField
      FieldName = 'Sexta'
    end
    object tbProgramasSabado: TSmallintField
      FieldName = 'Sabado'
    end
  end
  object tbLocutores: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Codigo'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Locutor'
        Attributes = [faRequired]
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Endereco'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'Bairro'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'Cidade'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'UF'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CEP'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'Telefone1'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Telefone2'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Email'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Cpf'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'RG'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'ValorTexto'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'Locutores01'
        Fields = 'Codigo'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'Locutores02'
        Fields = 'Locutor'
      end>
    StoreDefs = True
    TableName = 'LOCUTORES'
    Left = 168
    Top = 80
    object tbLocutoresCodigo: TIBStringField
      FieldName = 'Codigo'
      Required = True
      Size = 2
    end
    object tbLocutoresLocutor: TIBStringField
      FieldName = 'Locutor'
      Required = True
      Size = 40
    end
    object tbLocutoresEndereco: TIBStringField
      FieldName = 'Endereco'
      Size = 35
    end
    object tbLocutoresBairro: TIBStringField
      FieldName = 'Bairro'
      Size = 25
    end
    object tbLocutoresCidade: TIBStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object tbLocutoresUF: TIBStringField
      FieldName = 'UF'
      Size = 2
    end
    object tbLocutoresCEP: TIBStringField
      FieldName = 'CEP'
      Size = 9
    end
    object tbLocutoresTelefone1: TIBStringField
      FieldName = 'Telefone1'
      Size = 15
    end
    object tbLocutoresTelefone2: TIBStringField
      FieldName = 'Telefone2'
      Size = 15
    end
    object tbLocutoresEmail: TIBStringField
      FieldName = 'Email'
      Size = 30
    end
    object tbLocutoresCpf: TIBStringField
      FieldName = 'Cpf'
      Size = 14
    end
    object tbLocutoresRG: TIBStringField
      FieldName = 'RG'
      Size = 15
    end
    object tbLocutoresValorTexto: TFloatField
      FieldName = 'ValorTexto'
    end
  end
  object tbTabela: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Codigo'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Tabela'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'QtdeDia'
        DataType = ftInteger
      end
      item
        Name = 'QtdeMes'
        DataType = ftInteger
      end
      item
        Name = 'ValorMes'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'Tabela01'
        Fields = 'Codigo'
        Options = [ixPrimary, ixUnique]
      end>
    StoreDefs = True
    TableName = 'TABELA'
    Left = 464
    Top = 80
    object tbTabelaCodigo: TIBStringField
      FieldName = 'Codigo'
      Size = 2
    end
    object tbTabelaTabela: TIBStringField
      FieldName = 'Tabela'
      Size = 10
    end
    object tbTabelaQtdeDia: TIntegerField
      FieldName = 'QtdeDia'
    end
    object tbTabelaQtdeMes: TIntegerField
      FieldName = 'QtdeMes'
    end
    object tbTabelaValorMes: TFloatField
      FieldName = 'ValorMes'
    end
  end
  object tbAudiosProgramas: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Sequencia'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Audio'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Tipo'
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'AudiosProgramas01'
        Fields = 'Codigo;Sequencia'
        Options = [ixPrimary, ixUnique]
      end>
    StoreDefs = True
    TableName = 'AUDIOSPROGRAMAS'
    Left = 328
    Top = 80
    object tbAudiosProgramasCodigo: TIBStringField
      FieldName = 'Codigo'
      Size = 2
    end
    object tbAudiosProgramasSequencia: TIBStringField
      FieldName = 'Sequencia'
      Size = 2
    end
    object tbAudiosProgramasAudio: TIBStringField
      FieldName = 'Audio'
      Size = 8
    end
    object tbAudiosProgramasTipo: TIBStringField
      FieldName = 'Tipo'
      Size = 1
    end
  end
  object tbParamet: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'COCLIENTE'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'COPUBLICITARIO'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'COLOCUTOR'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'COPROGRAMA'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'COPRODUTO'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'COCONTRATO'
        DataType = ftString
        Size = 6
      end>
    StoreDefs = True
    TableName = 'PARAMETROS'
    Left = 24
    Top = 208
    object tbParametcoCliente: TIBStringField
      FieldName = 'coCliente'
      Size = 4
    end
    object tbParametcoPublicitario: TIBStringField
      FieldName = 'coPublicitario'
      Size = 3
    end
    object tbParametcoLocutor: TIBStringField
      FieldName = 'coLocutor'
      Size = 2
    end
    object tbParametcoPrograma: TIBStringField
      FieldName = 'coPrograma'
      Size = 2
    end
    object tbParametcoProduto: TIBStringField
      FieldName = 'coProduto'
      Size = 3
    end
    object tbParametcoContrato: TIBStringField
      FieldName = 'coContrato'
      Size = 6
    end
  end
  object tbContratos: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Numero'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Data'
        DataType = ftDate
      end
      item
        Name = 'Cliente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'DataInicio'
        DataType = ftDate
      end
      item
        Name = 'DataFinal'
        DataType = ftDate
      end
      item
        Name = 'TotalDias'
        DataType = ftInteger
      end
      item
        Name = 'Atividade'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'Publicitario'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'Comissao'
        DataType = ftFloat
      end
      item
        Name = 'TotalContrato'
        DataType = ftFloat
      end
      item
        Name = 'Descontos'
        DataType = ftFloat
      end
      item
        Name = 'Total'
        DataType = ftFloat
      end
      item
        Name = 'NumeroMeses'
        DataType = ftInteger
      end
      item
        Name = 'ValorContratoMensal'
        DataType = ftFloat
      end
      item
        Name = 'DescontosMensal'
        DataType = ftFloat
      end
      item
        Name = 'TotalMensal'
        DataType = ftFloat
      end
      item
        Name = 'Obs'
        DataType = ftMemo
      end>
    IndexDefs = <
      item
        Name = 'Contratos01'
        Fields = 'Numero'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'Contratos02'
        Fields = 'Cliente'
        Options = [ixCaseInsensitive]
      end>
    StoreDefs = True
    TableName = 'CONTRATOS'
    Left = 24
    Top = 152
    object tbContratosNumero: TIBStringField
      FieldName = 'Numero'
      Size = 6
    end
    object tbContratosData: TDateField
      FieldName = 'Data'
    end
    object tbContratosCliente: TIBStringField
      FieldName = 'Cliente'
      Size = 4
    end
    object tbContratoslkCliente: TStringField
      FieldKind = fkLookup
      FieldName = 'lkCliente'
      LookupDataSet = tbClientes
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Cliente'
      Size = 40
      Lookup = True
    end
    object tbContratosDataInicio: TDateField
      FieldName = 'DataInicio'
    end
    object tbContratosDataFinal: TDateField
      FieldName = 'DataFinal'
    end
    object tbContratosTotalDias: TIntegerField
      FieldName = 'TotalDias'
    end
    object tbContratosAtividade: TIBStringField
      FieldName = 'Atividade'
      Size = 3
    end
    object tbContratoslkAtividade: TStringField
      FieldKind = fkLookup
      FieldName = 'lkAtividade'
      LookupDataSet = tbProdutos
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Descricao'
      KeyFields = 'Atividade'
      Size = 40
      Lookup = True
    end
    object tbContratosPublicitario: TIBStringField
      FieldName = 'Publicitario'
      Size = 3
    end
    object tbContratoslkPublicitario: TStringField
      FieldKind = fkLookup
      FieldName = 'lkPublicitario'
      LookupDataSet = tbPublicitarios
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Publicitario'
      Size = 40
      Lookup = True
    end
    object tbContratosComissao: TFloatField
      FieldName = 'Comissao'
    end
    object tbContratosTotalContrato: TFloatField
      FieldName = 'TotalContrato'
    end
    object tbContratosDescontos: TFloatField
      FieldName = 'Descontos'
    end
    object tbContratosTotal: TFloatField
      FieldName = 'Total'
    end
    object tbContratosNumeroMeses: TIntegerField
      FieldName = 'NumeroMeses'
    end
    object tbContratosValorContratoMensal: TFloatField
      FieldName = 'ValorContratoMensal'
    end
    object tbContratosDescontosMensal: TFloatField
      FieldName = 'DescontosMensal'
    end
    object tbContratosTotalMensal: TFloatField
      FieldName = 'TotalMensal'
    end
    object tbContratosObs: TMemoField
      FieldName = 'Obs'
      BlobType = ftMemo
    end
  end
  object tbContratosCdAudios: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Contrato'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Audio'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Periodo1'
        DataType = ftDate
      end
      item
        Name = 'Periodo2'
        DataType = ftDate
      end
      item
        Name = 'Patrocinio'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'DiasPeriodo'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Horarios'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'VeiculacoesDia'
        DataType = ftInteger
      end
      item
        Name = 'VeiculacoesMes'
        DataType = ftInteger
      end
      item
        Name = 'TotalVeiculacoes'
        DataType = ftInteger
      end
      item
        Name = 'MesesVeiculacoes'
        DataType = ftInteger
      end
      item
        Name = 'Duracao'
        DataType = ftInteger
      end
      item
        Name = 'ValorBruto'
        DataType = ftFloat
      end
      item
        Name = 'Descontos'
        DataType = ftFloat
      end
      item
        Name = 'ValorLiquido'
        DataType = ftFloat
      end
      item
        Name = 'ValorLiquidoMensal'
        DataType = ftFloat
      end
      item
        Name = 'Posicao'
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'ContratosCdAudios01'
        Fields = 'Contrato;Audio'
        Options = [ixPrimary, ixUnique]
      end>
    StoreDefs = True
    TableName = 'CONTRATOSCDAUDIOS'
    Left = 112
    Top = 152
    object tbContratosCdAudiosContrato: TIBStringField
      FieldName = 'Contrato'
      Size = 6
    end
    object tbContratosCdAudiosAudio: TIBStringField
      FieldName = 'Audio'
      Size = 2
    end
    object tbContratosCdAudiosPeriodo1: TDateField
      FieldName = 'Periodo1'
    end
    object tbContratosCdAudiosPeriodo2: TDateField
      FieldName = 'Periodo2'
    end
    object tbContratosCdAudiosPatrocinio: TIBStringField
      FieldName = 'Patrocinio'
      Size = 2
    end
    object tbContratosCdAudioslkPatrocinio: TStringField
      FieldKind = fkLookup
      FieldName = 'lkPatrocinio'
      LookupDataSet = tbProgramas
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Programa'
      KeyFields = 'Patrocinio'
      Size = 30
      Lookup = True
    end
    object tbContratosCdAudiosDiasPeriodo: TIBStringField
      FieldName = 'DiasPeriodo'
      Size = 2
    end
    object tbContratosCdAudiosHorarios: TIBStringField
      FieldName = 'Horarios'
      Size = 1
    end
    object tbContratosCdAudiosVeiculacoesDia: TIntegerField
      FieldName = 'VeiculacoesDia'
    end
    object tbContratosCdAudiosVeiculacoesMes: TIntegerField
      FieldName = 'VeiculacoesMes'
    end
    object tbContratosCdAudiosTotalVeiculacoes: TIntegerField
      FieldName = 'TotalVeiculacoes'
    end
    object tbContratosCdAudiosMesesVeiculacoes: TIntegerField
      FieldName = 'MesesVeiculacoes'
    end
    object tbContratosCdAudiosDuracao: TIntegerField
      FieldName = 'Duracao'
    end
    object tbContratosCdAudiosValorBruto: TFloatField
      FieldName = 'ValorBruto'
    end
    object tbContratosCdAudiosDescontos: TFloatField
      FieldName = 'Descontos'
    end
    object tbContratosCdAudiosValorLiquido: TFloatField
      FieldName = 'ValorLiquido'
    end
    object tbContratosCdAudiosValorLiquidoMensal: TFloatField
      FieldName = 'ValorLiquidoMensal'
    end
    object tbContratosCdAudiosPosicao: TIBStringField
      FieldName = 'Posicao'
      Size = 1
    end
  end
  object tbContratosDtAudios: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Contrato'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'SequenciaAudio'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'coAudio'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Audio'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Comercial'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ArquivoAudio'
        DataType = ftString
        Size = 40
      end>
    IndexDefs = <
      item
        Name = 'ContratosDtAudios01'
        Fields = 'Contrato;SequenciaAudio;coAudio'
        Options = [ixPrimary, ixUnique]
      end>
    StoreDefs = True
    TableName = 'CONTRATOSDTAUDIOS'
    Left = 216
    Top = 152
    object tbContratosDtAudiosContrato: TIBStringField
      FieldName = 'Contrato'
      Size = 6
    end
    object tbContratosDtAudiosSequenciaAudio: TIBStringField
      FieldName = 'SequenciaAudio'
      Size = 2
    end
    object tbContratosDtAudioscoAudio: TIBStringField
      FieldName = 'coAudio'
      Size = 2
    end
    object tbContratosDtAudiosAudio: TIBStringField
      FieldName = 'Audio'
      Size = 8
    end
    object tbContratosDtAudiosComercial: TIBStringField
      FieldName = 'Comercial'
      Size = 1
    end
    object tbContratosDtAudiosArquivoAudio: TIBStringField
      FieldName = 'ArquivoAudio'
      Size = 40
    end
  end
  object tbContratosTextos: TIBTable
    Database = dbWorkRadio
    Transaction = ibtWorkRadio
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'Contrato'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'SequenciaAudio'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Audio'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Texto'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Locutor1'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Locutor2'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Locutor3'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Valor'
        DataType = ftFloat
      end
      item
        Name = 'DataGravacao'
        DataType = ftDate
      end
      item
        Name = 'Validade1'
        DataType = ftDate
      end
      item
        Name = 'Validade2'
        DataType = ftDate
      end
      item
        Name = 'ArquivoTexto'
        DataType = ftString
        Size = 40
      end>
    IndexDefs = <
      item
        Name = 'ContratosTextos01'
        Fields = 'Contrato;SequenciaAudio;Audio;Texto'
        Options = [ixPrimary, ixUnique]
      end>
    StoreDefs = True
    TableName = 'CONTRATOSTEXTOS'
    Left = 312
    Top = 152
    object tbContratosTextosContrato: TIBStringField
      FieldName = 'Contrato'
      Size = 6
    end
    object tbContratosTextosSequenciaAudio: TIBStringField
      FieldName = 'SequenciaAudio'
      Size = 2
    end
    object tbContratosTextosAudio: TIBStringField
      FieldName = 'Audio'
      Size = 2
    end
    object tbContratosTextosTexto: TIBStringField
      FieldName = 'Texto'
      Size = 2
    end
    object tbContratosTextosLocutor1: TIBStringField
      FieldName = 'Locutor1'
      Size = 2
    end
    object tbContratosTextosLocutor2: TIBStringField
      FieldName = 'Locutor2'
      Size = 2
    end
    object tbContratosTextosLocutor3: TIBStringField
      FieldName = 'Locutor3'
      Size = 2
    end
    object tbContratosTextosValor: TFloatField
      FieldName = 'Valor'
    end
    object tbContratosTextosDataGravacao: TDateField
      FieldName = 'DataGravacao'
    end
    object tbContratosTextosValidade1: TDateField
      FieldName = 'Validade1'
    end
    object tbContratosTextosValidade2: TDateField
      FieldName = 'Validade2'
    end
    object tbContratosTextosArquivoTexto: TIBStringField
      FieldName = 'ArquivoTexto'
      Size = 40
    end
  end
end
