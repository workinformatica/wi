{
 WI 2.0
  Fun��es Auxiliares para Delphi 3.0
  by Work Inform�tica (C) 1993-1998
}
UNIT WI;

INTERFACE

 Uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  WinTypes, WinProcs, StdCtrls, Mask, Grids, DBGrids, ExtCtrls, Buttons,
  Db, TypInfo, ComCtrls,
  Empresa, DateUtils, idGlobal, Math, StrUtils,
  IdHashMessageDigest;

 Type
  TCfgEmpresa = class
   Private
   Public
    DadosEmpresa: Array[1..14] of String;
    Function RazaoSocial: String;
    Function Fantasia: String;
    Function Endereco: String;
    Function Numero: String;
    Function Bairro: String;
    Function Cidade: String;
    Function Uf: String;
    Function Cep: String;
    Function Cgc: String;
    Function IE: String;
    Function Telefone1: String;
    Function Telefone2: String;
    Function Fax: String;
    Function Terminais: String;
   End;

// Fun��es convertidas Clipper/Delphi
function Left(InString: string; NumChars: integer): string;
function Right(InString: string; NumChars: integer): string;
function Len(InString: string): integer;
Function LTrim(Text : String) : String;
function Rtrim(Text : string) : string;
function Empty(InString: string): boolean;
function Substr(InString: string; Inicio, Quantos: integer): string;
function PutStr(InString, PutString: string; Onde: integer): string;
function Stuff(InString, PutString: string; Onde: integer): string;
function Upper(InString: string): string;
function Space(Quantos: integer): string;
function AT(Texto, Padrao: string): integer;
function ASC(InString: string): byte;
//function DayOfWeek(Dia:integer): string;
function Fix(Numero: double): double;
function RAD(Numero: double): double;
function DEG(Numero: double): double;
function LOG(Numero: double): double;
function TAN(Numero: double): double;
function AllTrim(InString: string): string;
function Padl(text:string;nqtde:integer):string;
function Padr(text:string;nqtde:integer):string;
function PadC(const S: string; const Len: integer): string;
function Replicate(Caractere : char; nCaracteres : integer) : string;
Function StrZero(vValor: Integer; vTamanho: Integer): String;
Function STRASCAN(const S: string; const List: array of string): Integer;

// Func�o Work Inform�tica
 Function Se(vCondicao, vVerdadeiro, vFalso: Variant): Variant;
 Function Imp_err(vMensagem: String): Boolean;
 Function Confirma(vModo: Integer; vMensagem: String): Boolean;
 Function Cp_Mes(vMes: String): String;
 Function Dms(vDms: Real): Real;
 Function Valor_Hora(vValorHora: TDateTime): String;
 Function Hora_Valor(vHoraValor: String): TDateTime;
 Function UsuarioRede: String;
 Function ComputadorRede: String;
 Function DesCriptografa(vTexto: String): String;
 Function Configura_Empresa: Boolean;
 Procedure Grava_CfgEmpresa;
 Function Mes_Extenso(vData: TDateTime): String;
 Function Dia_Maximo(vData: TDateTime): String;
 Function SerialDrive(Drive:String):String;
 Function Dtoi(vValorData:TDateTime): String;
 Function Ponto(pValor: Double; pTmh: Integer): String;
 Function VerCpf( var snrcpf:string):Boolean;
 Function VerCgc( var snrcgc:string):Boolean;
 Function FmtCpfCgc(vCpfCgc: String): String;
 Function ValidaCpfCgc(vCpfCgc: String): Boolean;
 Function IsNumero(vNro: String): Boolean;
 Function TiraPontos(Str: string): string;

// Fun��es matematicas
function TruncFix( X : Double ) : Integer;
function TruncFix3d( X : Double ) : Double;

// Fun��es MD5
function MD5String(const Value: string): string;
function MD5Arquivo(const Value: string): string;

// Variaveis de sistemas
Var vNroTerminal: String;

IMPLEMENTATION

////////////////////////////////////////////////////////////////////////////////
// FUN��ES CONVERTIDAS DO CLIPPER

function Left;
begin
 Left:=copy(InString,1,NumChars);
end;

function Right;
begin
 if NumChars >= length(InString) then
  Right:=InString
 else
  Right:=copy(InString,succ(length(InString)-NumChars),NumChars);
end;

function Len;
begin
 Len:=length(InString);
end;

Function LTrim;
var
   n : integer;
begin
  for n :=1 to length(Text) do
  begin
     if Copy(Text,n,1) <> ' ' then break;
     Delete(Text,n,1);
  end;
  Result := Text;
end;

function Rtrim(Text : string) : string;
var
  n : integer;
begin
  for n := length(Text) downto 1 do
  begin
       if Copy(Text,n,1) <> ' ' then break;
       Delete(Text,n,1);
  end;
  Result := Text;
end;

function Empty;
begin
 Empty:=RTrim(InString) = '';
end;

function Substr;
begin
 Substr:=copy(InString,Inicio,Quantos);
end;

function PutStr;
begin
 if Onde > length(InString) then
  InString:=InString + PutString
 else
  begin
   delete(InString,Onde,length(PutString));
   Insert(PutString,InString,Onde);
  end;
 PutStr:=InString;
end;

function Stuff;
begin
 Insert(PutString,InString,Onde);
 Stuff:=InString;
end;

function Upper;
begin
 Upper:=StrPas(StrUpper(PChar(InString)));
end;

function Space;
var
 TempStr: string;
begin
 TempStr:='';
 while length(TempStr) < Quantos do
  Insert(' ',TempStr,1);
 Space:=TempStr;
end;

function AT;
begin
 AT:=Pos(Padrao,Texto);
end;

function ASC;
begin
 if InString = '' then
  ASC:=0
 else
  ASC:=Ord(InString[1]);
end;

{function DayOfWeek;
begin
 DayOfWeek:=FormatSettings.LongDayNames[Dia+1];
end;}

function Fix;
begin
 Fix:=Numero - Frac(Numero);
end;

function RAD;
begin
 RAD:=Numero * (PI / 180);
end;

function DEG;
begin
 DEG:=Numero * (180 / PI);
end;

function LOG;
begin
 LOG:=LN(Numero);
end;

function TAN;
begin
 TAN:=Sin(Numero) / Cos(Numero);
end;

function AllTrim;
begin
  alltrim:=ltrim(rtrim(instring));
end;

function Replicate(Caractere : char; nCaracteres : integer) : string;
var n : integer; CadeiaCar : string;
begin
CadeiaCar := '';
for n := 1 to nCaracteres do
    CadeiaCar := CadeiaCar + Caractere;
Result := CadeiaCar;
end;

function PadL(text:string;nqtde:integer):string;
Begin
   PadL:=Replicate(' ',nQtde-Len(text))+alltrim(text);
end;

function PadR(text:string;nqtde:integer):string;
Begin
 If Length(text) < nqtde Then
   PadR:=alltrim(text)+Replicate(' ',nQtde-Len(alltrim(text)))
 Else
   PadR:=LeftStr(text,nqtde);
end;

function PadC(const S: string; const Len: integer): string;
var
I, J: integer;
Pad: string;
Impar: boolean;
begin
  I := Length(S);
  if I < Len then begin
    J := Len - I;
    Impar := J mod 2 = 1;
    J := J div 2;
    Pad := Replicate(' ', J);
    Result := Pad + S + Pad;
    if Impar then
    Result := Result + ' ';
  end else if I > Len then begin
    J := I - Len;
    Impar := J mod 2 = 1;
    J := J div 2;
    Result := S;
    Delete(Result, I-J+1, J);
    Delete(Result, 1, J);
    if Impar then begin
      Dec(I, J * 2);
      Delete(Result, I, 1);
    end;
  end else
  Result := S;
end;


{
 Strzero()
  Fun��o p/ Retornar zeros a esquerda
}
Function StrZero(vValor: Integer; vTamanho: Integer): String;
Var vConverte: String; vContador: Integer;
Begin
 vConverte := '';

 For vContador := 1 to (vTamanho-Length(InttoStr(vValor))) do
 Begin
  vConverte := vConverte + '0';
 End;

 Result := vConverte + InttoStr(vValor);
End;

{
 STRASCAN()
  Obter o �ndice de um valor string em um array din�mico de strings.
}
Function STRASCAN(const S: string; const List: array of string): Integer;
var
  I: Integer;
begin
  Result := 0;

  for I := Low(List) to High(List) do
  begin
    if AnsiSameText(S, List[I]) then
    begin
      Result := I + 1;
      Break;
    end;
  end;
end;


////////////////////////////////////////////////////////////////////////////////
// FUN��ES CONVERTIDAS DO ARQUIVO DE DESENVOLVIMENTO DE FUNCOES WORK INFORMATICA

{
 Se()
  Fun��o de Compara��o
  Exemplo: Se(100>0,'Verdadeiro','Falso') Retorna 'Verdadeiro'
}
Function Se(vCondicao, vVerdadeiro, vFalso: Variant): Variant;
Begin
 If (vCondicao) Then
    Result := vVerdadeiro
 Else
    Result := vFalso;
End;


{
 Imp_err()
  Fun��o p/ exibir mensagem de aviso
}
Function Imp_err(vMensagem: String): Boolean;
Begin
 MessageDlg(vMensagem, mtInformation, [mbOk], 0);
 Result := True;
End;


{
 Confirma()
  Fun��o p/ Exibir mensagem de confirma��o
}
Function Confirma(vModo: Integer; vMensagem: String): Boolean;
Begin
 If vModo = 1 Then
    vMensagem := 'Confirma os Dados Acimas ?';
 If vModo = 2 Then
    vMensagem := 'Continuar o Processamento ?';
 If vModo = 3 Then
    vMensagem := 'Tem certeza que deseja Excluir este Registro ?';
 If vModo = 4 Then
    vMensagem := 'Imprimir diretamente na Impressora ?';
 If vModo = 5 Then
    vMensagem := 'Confirma a Impress�o dos Dados ?';
If vModo = 6 Then
    vMensagem := 'Cancelar Venda?';
 If MessageDlg(vMensagem,mtConfirmation,[mbYes,mbNo],0) = mrYes Then
    Result := True
 Else
    Result := False;
End;


{
 Cp_Mes()
  Fun��o p/ Compactar m�s.
  Exemplo: Cp_Mes('01/1998') Retorna '199801'
           Cp_Mes('199801') Retorna '01/1998'
}
Function Cp_Mes(vMes: String): String;
Begin
 If Len(Alltrim(vMes)) = 7 Then
    Result := Copy(vMes,4,4)+Copy(vMes,1,2)
 Else
    Result := Copy(vMes,5,2)+'/'+Copy(vMes,1,4);

End;


{
 DMS()
  Funcao p/ Transformar de Decimal para vDeg de Hora, Minuto e Segundo
}
Function Dms(vDms: Real): Real;
Begin
// Result := Int(vDms) + Int(((vDms-Int(vDms))+8.1*10^-7)*60) / 100+((((vDms-Int(vDms))*60)-Int((vDms-Int(vDms))*60))*60)/10000;
 Result := vDms;
End;


{
 VALOR_HORA()
  Funcao p/ Transformar Hora em Formato Hora String 99:00
}
Function Valor_Hora(vValorHora: TDateTime): String;
Var xValorHora: String;
Begin
 xValorHora := TimetoStr(vValorHora);
 Delete(xValorHora,6,3);
 Result := xValorHora;
End;


{
 HORA_VALOR()
  Funcao p/ Transformar Hora String no Formato Valor de TDateTime
}
Function Hora_Valor(vHoraValor: String): TDateTime;
Begin
 Result := StrtoTime(vHoraValor);
End;


{
 UsuarioRede()
  Fun��o que Retorna o Nome do Usu�rio em Rede
}
Function UsuarioRede: String;
Var lpBuffer : Array[0..20] of Char;
    nSize: dWord;
    Achou: boolean;
    erro: dWord;
Begin
 nSize := 120;
 Achou := GetUserName(lpBuffer,nSize);
 If (Achou) then Begin
    Result := Alltrim(Upper(lpBuffer))
  End
 Else begin
   Erro   :=GetLastError();
   result :=IntToStr(Erro);
  End;

End;

{
 ComputadorRede()
  Fun��o que Retorna o Nome do Computador em Rede
}
Function ComputadorRede: String;
Var lpBuffer : Array[0..20] of Char;
    nSize: dWord;
    Achou: boolean;
    erro: dWord;
Begin
 nSize := 120;
 Achou := GetComputerName(lpBuffer,nSize);
 If (Achou) then Begin
    Result := Alltrim(Upper(lpBuffer))
  End
 Else begin
   Erro   :=GetLastError();
   result :=IntToStr(Erro);
  End;
End;

{
 Fun��o p/ Criptografar Dados
}
Function Criptografa(vTexto: String): String;
Var vCript: String;
    vAsc: Byte;
    cCript: Integer;
Begin
 vCript := '';

 For cCript := 1 to Length(vTexto) do Begin
  vAsc := Asc(Copy(vTexto, cCript, 1));
  vCript := vCript + Chr((176+vAsc)-40);
 End;

 Result := vCript;
End;


{
 Fun��o p/ DesCriptografar Dados
}
Function DesCriptografa(vTexto: String): String;
Var vDesCript: String;
    vAsc: Byte;
    cDesCript: Integer;
Begin
 vDesCript := '';

 For cDesCript := 1 to Length(vTexto) do Begin
  vAsc := Asc(Copy(vTexto, cDesCript, 1));
  vDesCript := vDesCript + Chr((vAsc-176)+40);
 End;

 Result := vDesCript;
End;


{
 Fun��o p/ Configura��o da Empresa Usuaria do Sistema
}
Function Configura_Empresa: Boolean;
Var vArquivo: TextFile;
    vConteudo: String;
    vTamanhos: Array[1..14] of Integer;
    cCfg: Integer;
    vPosicao: Integer;
    vDados: TCfgEmpresa;
Begin
 Result := True;

 vTamanhos[1]  := 40;
 vTamanhos[2]  := 40;
 vTamanhos[3]  := 35;
 vTamanhos[4]  := 5;
 vTamanhos[5]  := 20;
 vTamanhos[6]  := 20;
 vTamanhos[7]  := 2;
 vTamanhos[8]  := 9;
 vTamanhos[9]  := 18;
 vTamanhos[10] := 12;
 vTamanhos[11] := 14;
 vTamanhos[12] := 14;
 vTamanhos[13] := 14;
 vTamanhos[14] := 12;

{
 If not FileExists('C:\PROJETOS\EMPRESA.INI') and Operador_p # 'SUPERVISOR' Then Begin
    Imp_err(', Arquivo de Configura��o de Empresa Usuaria n�o Encontrada !');
    Result := False;
 End;
}

 If not FileExists('C:\PROJETOS\EMPRESA.INI') Then Begin
    Application.CreateForm(TfrmCfgEmpresa, frmCfgEmpresa);
    frmCfgEmpresa.Show;
    Exit;
 End;

 If FileExists('C:\PROJETOS\EMPRESA.INI') Then Begin
    AssignFile(vArquivo, 'C:\PROJETOS\EMPRESA.INI');
    Reset(vArquivo);
    Readln(vArquivo, vConteudo);
    CloseFile(vArquivo);

    vPosicao := 1;
    For cCfg := 1 to 14 do Begin
     vDados.DadosEmpresa[cCfg] := Descriptografa(Copy(vConteudo, vPosicao, vTamanhos[cCfg]));
     vPosicao := vPosicao + vTamanhos[cCfg];
    End;

 End;
End;

Procedure Grava_CfgEmpresa;
Var vArquivo: TextFile;
    cCfg: Integer;
    vDados: TCfgEmpresa;
    vConteudo: String;
Begin
 vConteudo := '';
 AssignFile(vArquivo, 'C:\PROJETOS\EMPRESA.INI');

 If not FileExists('C:\PROJETOS\EMPRESA.INI') Then Begin
    ReWrite(vArquivo);
 End;

 If FileExists('C:\PROJETOS\EMPRESA.INI') Then Begin
    Reset(vArquivo);
 End;

 For cCfg := 1 to 14 do Begin
  vConteudo := vConteudo+Criptografa(vDados.DadosEmpresa[cCfg]);
 End;

 Writeln(vArquivo,vConteudo);
 CloseFile(vArquivo);
End;


Function TCfgEmpresa.RazaoSocial: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[1];
End;

Function TCfgEmpresa.Fantasia: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[2];
End;

Function TCfgEmpresa.Endereco: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[3];
End;

Function TCfgEmpresa.Numero: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[4];
End;

Function TCfgEmpresa.Bairro: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[5];
End;

Function TCfgEmpresa.Cidade: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[6];
End;

Function TCfgEmpresa.Uf: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[7];
End;

Function TCfgEmpresa.Cep: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[8];
End;

Function TCfgEmpresa.Cgc: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[9];
End;

Function TCfgEmpresa.IE: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[10];
End;

Function TCfgEmpresa.Telefone1: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[11];
End;

Function TCfgEmpresa.Telefone2: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[12];
End;

Function TCfgEmpresa.Fax: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[13];
End;

Function TCfgEmpresa.Terminais: String;
Var vDados: TCfgEmpresa;
Begin
 Result := vDados.DadosEmpresa[14];
End;


{
 Mes_Extenso()
  Retorna o mes por extenso
}
Function Mes_Extenso(vData: TDateTime): String;
Var vMeses: Array[1..12] of String;
Begin
 vMeses[1] := 'Janeiro';
 vMeses[2] := 'Fevereiro';
 vMeses[3] := 'Marco';
 vMeses[4] := 'Abril';
 vMeses[5] := 'Maio';
 vMeses[6] := 'Junho';
 vMeses[7] := 'Julho';
 vMeses[8] := 'Agosto';
 vMeses[9] := 'Setembro';
 vMeses[10] := 'Outubro';
 vMeses[11] := 'Novembro';
 vMeses[12] := 'Dezembro';

 Result := vMeses[StrtoInt(Copy(DatetoStr(vData),4,2))];
End;


{
 Dia_Maximo()
  Retorna o maior dia do mes
}
Function Dia_Maximo(vData: TDateTime): String;
Var vDia: Array[1..12] of String;
Begin
 vDia[1] := '31';
 vDia[2] := '28';
 If (YearOf(vData) / 4) = Int(YearOf(vData) / 4) Then
    vDia[2] := '29';
 vDia[3] := '31';
 vDia[4] := '30';
 vDia[5] := '31';
 vDia[6] := '30';
 vDia[7] := '31';
 vDia[8] := '31';
 vDia[9] := '30';
 vDia[10] := '31';
 vDia[11] := '30';
 vDia[12] := '31';

 Result := vDia[StrtoInt(Copy(DatetoStr(vData),4,2))];
End;


{
 DiskSerial()
  Retorna o Numero de S�rie do Drive Informado
}
Function SerialDrive(Drive:String):String;
Var Serial:DWord;
    DirLen,Flags: DWord;
    DLabel : Array[0..11] of Char;
Begin
 GetVolumeInformation(PChar(Drive+':\'),dLabel,12,@Serial,DirLen,Flags,nil,0);

 Result := IntToHex(Serial,8);
End;

Function Dtoi(vValorData:TDateTime): String;
Var xDATA: String;
Begin
 xDATA := DatetoStr(vValorData);
 Result := Right(xDATA,Se(Len(xDATA)=8,2,4))+Substr(xDATA,4,2)+Left(xDATA,2);
End;

Function Ponto(pValor: Double; pTmh: Integer): String;
Begin
 Ponto := Padl(FloattoStrf(pValor,ffNumber,pTmh,2), pTmh);
End;

// Valida CPF
Function VerCpf( var snrcpf:string):Boolean;
VAR
   WCPFCALC : STRING;
   WSOMACPF : INTEGER;
   WSX1     : SHORTINT;
   WCPFDIGT : INTEGER;
begin
 Result := False;
 Try
   if snrcpf <> '   .   .   -  ' then
       BEGIN
	 snrCpf := Copy(snrcpf,1,3)+Copy(snrcpf,5,3)+
     Copy(snrcpf,9,3)+Copy(snrcpf,13,2);

	wcpfcalc := copy(snrCpf, 1, 9);
	wsomacpf := 0;
	for wsx1:= 1 to 9 DO
	     wsomacpf := wsomacpf + strtoint(copy(wcpfcalc, wsx1, 1)) * (11 - wsx1);
	wcpfdigt:= 11 - wsomacpf mod 11;
	if wcpfdigt in [10,11] then
	    BEGIN
	      wcpfcalc:= wcpfcalc + '0';
	    END
	else
	    BEGIN
	      wcpfcalc := wcpfcalc +  inttoStr(wcpfdigt);
	    END;
	wsomacpf:= 0;
	for wsx1:= 1 to 10 DO
	     wsomacpf := wsomacpf + strtoint(copy(wcpfcalc, wsx1, 1)) * (12 - wsx1);
	wcpfdigt:= 11 - wsomacpf mod 11;
	if wcpfdigt in [10,11] then
	    BEGIN
	      wcpfcalc:= wcpfcalc + '0';
	    END
	else
	    BEGIN
	      wcpfcalc := wcpfcalc +  inttoStr(wcpfdigt);
	    END;

	if  snrcpf <> wcpfcalc then
	  Begin
	   application.messagebox('Este C.P.F. esta errado !','Aten��o!',mb_iconstop+mb_ok);
	   Result:=False;
	  end
	else
	   Result :=True;
    END
  except
    application.messagebox('Este C.P.F. esta errado !','Aten��o!',mb_iconstop+mb_ok);
    Result:=False;
  end
end;

// Valida CGC
Function VerCgc( var snrcgc:string):Boolean;
VAR
   WCGCCALC : STRING;
   WSOMACGC : INTEGER;
   WSX1     : SHORTINT;
   WCGCDIGT : INTEGER;
begin
 Result := False;
 try
  if snrCGC <> '  .   .   /    -  ' then
     BEGIN
	 snrcgc := Copy(snrcgc,1,2)+Copy(snrcgc,4,3)+
	   Copy(snrcgc,8,3)+Copy(snrcgc,12,4)+Copy(snrcgc,17,2);
	 wCgcCalc := Copy(snrcgc,1,12);
	 WSOMACGC := 0;
	 {-----------------------------}
	 for wsx1:= 1 to 4 do
	      wsomacgc:= wsomacgc + strtoint(copy(wcgccalc, wsx1, 1)) * (6 - wsx1);
	 for wsx1:= 1 to 8 do
	      wsomacgc:= wsomacgc + strtoint(copy(wcgccalc, wsx1 + 4, 1)) * (10 - wsx1);
	 wcgcdigt:= 11 - wsomacgc mod 11;
	 if wcgcdigt in [10,11] then
	     BEGIN
	       wcgccalc:= wcgccalc + '0';
	     END
	 else
	     BEGIN
	       wcgccalc := wcgccalc +  inttoStr(wcgcdigt);
	     END;
	 {---------------------------------}
	 wsomacgc:= 0;
	 for wsx1:= 1 to 5 do
	      wsomacgc:= wsomacgc + strtoint(copy(wcgccalc, wsx1, 1)) * (7 - wsx1);
	 for wsx1:= 1 to 8 do
	      wsomacgc:= wsomacgc + strtoint(copy(wcgccalc, wsx1 + 5, 1)) * (10 - wsx1);
	 wcgcdigt:= 11 - wsomacgc mod 11;
	 if wcgcdigt in [10,11] then
	     BEGIN
	       wcgccalc:= wcgccalc + '0';
	     END
	 else
	     BEGIN
	       wcgccalc := wcgccalc +  inttoStr(wcgcdigt);
	     END;
	 if  snrcgc <> wcgccalc then
	  Begin
	   application.messagebox('Este C.G.C. esta errado!','Aten��o!',mb_iconstop+mb_ok);
	   Result:=False;
	  end
	 else
	  Result:=True;
      END
  except
    application.messagebox('Este C.G.C. esta errado!','Aten��o!',mb_iconstop+mb_ok);
    Result:=False;
  end;
end;

// Formata CPF/CGC
Function FmtCpfCgc(vCpfCgc: String): String;
Begin
 Result := vCpfCgc;

 If not IsNumero(vCpfCgc) Then Begin
    If (Len(Alltrim(vCpfCgc))<>14) and
       (Len(Alltrim(vCpfCgc))<>18) Then
       Imp_err(', Verifique CNPJ/CPF digitado !');
    Exit;
  End

 Else Begin
    If (Len(Alltrim(vCpfCgc))<>11) and
       (Len(Alltrim(vCpfCgc))<>14) Then Begin
       Imp_err(', Verifique CNPJ/CPF digitado !');
       Exit;
    End
  End;

 // CPF - 999.999.999-99
 If Len(Alltrim(vCpfCgc)) = 11 Then Begin
    vCpfCgc := Stuff(vCpfCgc,'.',4);
    vCpfCgc := Stuff(vCpfCgc,'.',8);
    vCpfCgc := Stuff(vCpfCgc,'-',12);
    Result := vCpfCgc;
    Exit;
 End;

 // CGC - 99.999.999/9999-99
 If Len(Alltrim(vCpfCgc)) = 14 Then Begin
    vCpfCgc := Stuff(vCpfCgc,'.',3);
    vCpfCgc := Stuff(vCpfCgc,'.',7);
    vCpfCgc := Stuff(vCpfCgc,'/',11);
    vCpfCgc := Stuff(vCpfCgc,'-',16);
    Result := vCpfCgc;
    Exit;
 End
End;

// Valida Cpf e Cgc
Function ValidaCpfCgc(vCpfCgc: String): Boolean;
Begin
 Result := False;

 // Cpf 999 999 999 99
 If Len(Alltrim(vCpfCgc)) = 14 Then
    Result := VerCpf(vCpfCgc)

 // Cgc 99 999 999 9999 99
 Else If Len(Alltrim(vCpfCgc)) = 18 Then
    Result := VerCgc(vCpfCgc);
{
 Else
    Imp_err(', Verifique CNPJ/CPF digitado !');
}
End;


Function IsNumero(vNro: String): Boolean;
Var x: integer;
Begin
 Result := True;

 For x:=1 to Len(vNro) do
  Try
   StrToInt(SubStr(vNro,x,1));
  Except
   On EConvertError do
      Result := False;
  End
End;

function TiraPontos(Str: string): string;
var
  i: Integer;
  xStr : String;
begin
 xStr := '';
 for i:=1 to Length(Trim(str)) do
   if (Pos(Copy(str,i,1),'/-.)(,')=0) then xStr := xStr + str[i];

 xStr := StringReplace(xStr,' ','',[rfReplaceAll]);

 Result:=xStr;
end;

function TruncFix( X : Double ) : Integer;
begin
  Result := Trunc( SimpleRoundTo( X, -9) );
end;

function TruncFix3d( X : Double ) : Double;
Begin
 Result := Trunc(X * 1000) / 1000;
End;


function MD5String(const Value: string): string;
var
  xMD5: TIdHashMessageDigest5;
begin
  xMD5 := TIdHashMessageDigest5.Create;
  try
    Result := xMD5.HashStringAsHex(Value);
  finally
    xMD5.Free;
  end;
end;

function MD5Arquivo(const Value: string): string;
var
  xMD5: TIdHashMessageDigest5;
  xArquivo: TFileStream;
begin
  xMD5 := TIdHashMessageDigest5.Create;
  xArquivo := TFileStream.Create(Value, fmOpenRead OR fmShareDenyWrite);
  try
    Result := xMD5.HashStreamAsHex(xArquivo);
  finally
    xArquivo.Free;
    xMD5.Free;
  end;
end;

END.

