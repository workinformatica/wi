unit Padrao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Buttons, ExtCtrls, Db;

type
  TfrmPadrao = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DBNavigator1: TDBNavigator;
    edPesquisa: TEdit;
    Label1: TLabel;
    cbOrdem: TComboBox;
    Label2: TLabel;
    DataSource1: TDataSource;
    Panel3: TPanel;
    btIncluir: TSpeedButton;
    btGravar: TSpeedButton;
    btCancelar: TSpeedButton;
    btExcluir: TSpeedButton;
    procedure DataSource1StateChange(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPadrao: TfrmPadrao;

implementation

{$R *.DFM}




End;

procedure TfrmPadrao.DataSource1StateChange(Sender: TObject);
begin
 btIncluir.Enabled  := If(frmTabelas.tbAlunos.State in [dsEdit,dsInsert], False, True);
 btGravar.Enabled   := Se(frmTabelas.tbAlunos.State in [dsEdit,dsInsert], True, False);
 btCancelar.Enabled := Se(frmTabelas.tbAlunos.State in [dsEdit,dsInsert], True, False);
 btExcluir.Enabled  := Se(frmTabelas.tbAlunos.State in [dsEdit,dsInsert], False, True);
end;

procedure TfrmPadrao.FormKeyPress(Sender: TObject; var Key: Char);
begin
 if key = #13 then begin
    key := #0;
    self.Perform(wm_nextdlgctl,0,0);
 end;
end;

end.
