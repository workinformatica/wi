unit unDmWorkRadio;

interface

uses
  SysUtils, Classes, DBXpress, DB, SqlExpr, FMTBcd, DBClient, Provider,
  DBLocal, DBLocalS, IBCustomDataSet, IBTable, IBDatabase;

type
  TdmWorkRadio = class(TDataModule)
    tbClientes: TIBTable;
    dbWorkRadio: TIBDatabase;
    ibtWorkRadio: TIBTransaction;
    tbProdutos: TIBTable;
    tbPublicitarios: TIBTable;
    tbProgramas: TIBTable;
    tbLocutores: TIBTable;
    tbTabela: TIBTable;
    tbProdutosCodigo: TIBStringField;
    tbProdutosDescricao: TIBStringField;
    tbLocutoresCodigo: TIBStringField;
    tbLocutoresLocutor: TIBStringField;
    tbLocutoresEndereco: TIBStringField;
    tbLocutoresBairro: TIBStringField;
    tbLocutoresCidade: TIBStringField;
    tbLocutoresUF: TIBStringField;
    tbLocutoresCEP: TIBStringField;
    tbLocutoresTelefone1: TIBStringField;
    tbLocutoresTelefone2: TIBStringField;
    tbLocutoresCpf: TIBStringField;
    tbLocutoresRG: TIBStringField;
    tbLocutoresValorTexto: TFloatField;
    tbTabelaCodigo: TIBStringField;
    tbTabelaTabela: TIBStringField;
    tbTabelaQtdeDia: TIntegerField;
    tbTabelaQtdeMes: TIntegerField;
    tbTabelaValorMes: TFloatField;
    tbLocutoresEmail: TIBStringField;
    tbPublicitariosCodigo: TIBStringField;
    tbPublicitariosNome: TIBStringField;
    tbPublicitariosEndereco: TIBStringField;
    tbPublicitariosBairro: TIBStringField;
    tbPublicitariosCidade: TIBStringField;
    tbPublicitariosUF: TIBStringField;
    tbPublicitariosCEP: TIBStringField;
    tbPublicitariosTele1: TIBStringField;
    tbPublicitariosTele2: TIBStringField;
    tbPublicitariosEmail: TIBStringField;
    tbPublicitariosCpf: TIBStringField;
    tbPublicitariosRG: TIBStringField;
    tbPublicitariosComissao1: TFloatField;
    tbPublicitariosComissao2: TFloatField;
    tbClientesCodigo: TIBStringField;
    tbClientesNome: TIBStringField;
    tbClientesRazaoSocial: TIBStringField;
    tbClientesEndereco: TIBStringField;
    tbClientesBairro: TIBStringField;
    tbClientesCidade: TIBStringField;
    tbClientesUF: TIBStringField;
    tbClientesCEP: TIBStringField;
    tbClientesTelefone1: TIBStringField;
    tbClientesTelefone2: TIBStringField;
    tbClientesFax: TIBStringField;
    tbClientesSite: TIBStringField;
    tbClientesEmail: TIBStringField;
    tbClientesCnpj_Cpf: TIBStringField;
    tbClientesIE_RG: TIBStringField;
    tbClientesResponsavel: TIBStringField;
    tbClientesCpf_resp: TIBStringField;
    tbClientesRG_resp: TIBStringField;
    tbClientesEmail_resp: TIBStringField;
    tbClientesAniversario_resp: TIBStringField;
    tbClientesEndereco_corresp: TIBStringField;
    tbClientesBairro_corresp: TIBStringField;
    tbClientesCidade_corresp: TIBStringField;
    tbClientesUF_corresp: TIBStringField;
    tbClientesCEP_corresp: TIBStringField;
    tbClientesSituacao: TIBStringField;
    tbAudiosProgramas: TIBTable;
    tbProgramasCodigo: TIBStringField;
    tbProgramasPrograma: TIBStringField;
    tbProgramasHorario: TTimeField;
    tbProgramasAudioAbertura: TIBStringField;
    tbProgramasComerciais: TIBStringField;
    tbProgramasPosicaoBloco: TIBStringField;
    tbProgramasAudioFinal: TIBStringField;
    tbAudiosProgramasCodigo: TIBStringField;
    tbAudiosProgramasSequencia: TIBStringField;
    tbAudiosProgramasAudio: TIBStringField;
    tbAudiosProgramasTipo: TIBStringField;
    tbParamet: TIBTable;
    tbParametcoCliente: TIBStringField;
    tbParametcoPublicitario: TIBStringField;
    tbParametcoLocutor: TIBStringField;
    tbParametcoPrograma: TIBStringField;
    tbParametcoProduto: TIBStringField;
    tbParametcoContrato: TIBStringField;
    tbProgramasDomingo: TSmallintField;
    tbProgramasSegunda: TSmallintField;
    tbProgramasTerca: TSmallintField;
    tbProgramasQuarta: TSmallintField;
    tbProgramasQuinta: TSmallintField;
    tbProgramasSexta: TSmallintField;
    tbProgramasSabado: TSmallintField;
    tbContratos: TIBTable;
    tbContratosNumero: TIBStringField;
    tbContratosData: TDateField;
    tbContratosCliente: TIBStringField;
    tbContratosDataInicio: TDateField;
    tbContratosDataFinal: TDateField;
    tbContratosTotalDias: TIntegerField;
    tbContratosAtividade: TIBStringField;
    tbContratosPublicitario: TIBStringField;
    tbContratosComissao: TFloatField;
    tbContratosTotalContrato: TFloatField;
    tbContratosDescontos: TFloatField;
    tbContratosTotal: TFloatField;
    tbContratosNumeroMeses: TIntegerField;
    tbContratosValorContratoMensal: TFloatField;
    tbContratosDescontosMensal: TFloatField;
    tbContratosTotalMensal: TFloatField;
    tbContratosObs: TMemoField;
    tbContratoslkCliente: TStringField;
    tbContratoslkPublicitario: TStringField;
    tbContratoslkAtividade: TStringField;
    tbContratosCdAudios: TIBTable;
    tbContratosDtAudios: TIBTable;
    tbContratosTextos: TIBTable;
    tbContratosCdAudiosContrato: TIBStringField;
    tbContratosCdAudiosAudio: TIBStringField;
    tbContratosCdAudiosPeriodo1: TDateField;
    tbContratosCdAudiosPeriodo2: TDateField;
    tbContratosCdAudiosPatrocinio: TIBStringField;
    tbContratosCdAudiosDiasPeriodo: TIBStringField;
    tbContratosCdAudiosHorarios: TIBStringField;
    tbContratosCdAudiosVeiculacoesDia: TIntegerField;
    tbContratosCdAudiosVeiculacoesMes: TIntegerField;
    tbContratosCdAudiosTotalVeiculacoes: TIntegerField;
    tbContratosCdAudiosMesesVeiculacoes: TIntegerField;
    tbContratosCdAudiosDuracao: TIntegerField;
    tbContratosCdAudiosValorBruto: TFloatField;
    tbContratosCdAudiosDescontos: TFloatField;
    tbContratosCdAudiosValorLiquido: TFloatField;
    tbContratosCdAudiosValorLiquidoMensal: TFloatField;
    tbContratosCdAudiosPosicao: TIBStringField;
    tbContratosDtAudiosContrato: TIBStringField;
    tbContratosDtAudiosSequenciaAudio: TIBStringField;
    tbContratosDtAudioscoAudio: TIBStringField;
    tbContratosDtAudiosAudio: TIBStringField;
    tbContratosDtAudiosComercial: TIBStringField;
    tbContratosDtAudiosArquivoAudio: TIBStringField;
    tbContratosTextosContrato: TIBStringField;
    tbContratosTextosSequenciaAudio: TIBStringField;
    tbContratosTextosAudio: TIBStringField;
    tbContratosTextosTexto: TIBStringField;
    tbContratosTextosLocutor1: TIBStringField;
    tbContratosTextosLocutor2: TIBStringField;
    tbContratosTextosLocutor3: TIBStringField;
    tbContratosTextosValor: TFloatField;
    tbContratosTextosDataGravacao: TDateField;
    tbContratosTextosValidade1: TDateField;
    tbContratosTextosValidade2: TDateField;
    tbContratosTextosArquivoTexto: TIBStringField;
    tbContratosCdAudioslkPatrocinio: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmWorkRadio: TdmWorkRadio;

implementation

uses unWorkRadio;

{$R *.dfm}

procedure TdmWorkRadio.DataModuleCreate(Sender: TObject);
Var vArq: TextFile;
    vCfgDatabase: String;
Begin
 AssignFile(vArq,'WorkRadio.Ini');
 Reset(vArq);
 Readln(vArq,vCfgDatabase); // L1
 vCfgDatabase := Copy(vCfgDatabase, 14, 40);
 CloseFile(vArq);

 dbWorkRadio.DatabaseName := vCfgDatabase;

 dbWorkRadio.Open;
 ibtWorkRadio.Active := True;

 If not tbClientes.Exists Then
    tbClientes.CreateTable;
 tbClientes.Open;

 If not tbProdutos.Exists Then
    tbProdutos.CreateTable;
 tbProdutos.Open;

 If not tbPublicitarios.Exists Then
    tbPublicitarios.CreateTable;
 tbPublicitarios.Open;

 If not tbLocutores.Exists Then
    tbLocutores.CreateTable;
 tbLocutores.Open;

 If not tbProgramas.Exists Then
    tbProgramas.CreateTable;
 tbProgramas.Open;

 If not tbAudiosProgramas.Exists Then
    tbAudiosProgramas.CreateTable;
 tbAudiosProgramas.Open;

 If not tbContratos.Exists Then
    tbContratos.CreateTable;
 tbContratos.Open;

 If not tbContratosCdAudios.Exists Then
    tbContratosCdAudios.CreateTable;
 tbContratosCdAudios.Open;

 If not tbContratosDtAudios.Exists Then
    tbContratosDtAudios.CreateTable;
 tbContratosDtAudios.Open;

 If not tbContratosTextos.Exists Then
    tbContratosTextos.CreateTable;
 tbContratosTextos.Open;

 If not tbTabela.Exists Then
    tbTabela.CreateTable;
 tbTabela.Open;

 If not tbParamet.Exists Then
    tbParamet.CreateTable;
 tbParamet.Open;

 dbWorkRadio.GetTableNames(fmWorkRadio.ListBox1.Items,False);
end;

end.

