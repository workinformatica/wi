object Form1: TForm1
  Left = 192
  Top = 107
  Width = 544
  Height = 375
  Caption = 'Cadastro de Procedimentos M�dicos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 536
    Height = 49
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 273
      Top = 8
      Width = 46
      Height = 13
      Caption = '&Pesquisa:'
      FocusControl = edPesquisa
    end
    object Label2: TLabel
      Left = 176
      Top = 8
      Width = 34
      Height = 13
      Caption = '&Ordem:'
      FocusControl = cbOrdem
    end
    object DBNavigator1: TDBNavigator
      Left = 1
      Top = 1
      Width = 168
      Height = 47
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      Align = alLeft
      TabOrder = 0
    end
    object edPesquisa: TEdit
      Left = 273
      Top = 24
      Width = 116
      Height = 21
      TabOrder = 1
    end
    object cbOrdem: TComboBox
      Left = 176
      Top = 24
      Width = 89
      Height = 21
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 299
    Width = 536
    Height = 49
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object btIncluir: TBitBtn
      Left = 102
      Top = 2
      Width = 75
      Height = 46
      Hint = 'Incluir um Novo Registro'
      Caption = '&Incluir'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object btGravar: TBitBtn
      Left = 179
      Top = 2
      Width = 75
      Height = 46
      Hint = 'Grava os Dados'
      Caption = '&Gravar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
        00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
        00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
        00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
        0003737FFFFFFFFF7F7330099999999900333777777777777733}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object btCancelar: TBitBtn
      Left = 256
      Top = 2
      Width = 75
      Height = 46
      Hint = 'Cancelar a Edi��o Atual'
      Cancel = True
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object btExcluir: TBitBtn
      Left = 333
      Top = 2
      Width = 75
      Height = 46
      Hint = 'Excluir o Registro Atual'
      Caption = '&Excluir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        300033FFFFFF3333377739999993333333333777777F3333333F399999933333
        3300377777733333337733333333333333003333333333333377333333333333
        3333333333333333333F333333333333330033333F33333333773333C3333333
        330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
        333333377F33333333FF3333C333333330003333733333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 49
    Width = 536
    Height = 250
    Align = alClient
    TabOrder = 2
    object Label3: TLabel
      Left = 11
      Top = 14
      Width = 33
      Height = 13
      Caption = 'Codigo'
      FocusControl = DBEdit1
    end
    object Label4: TLabel
      Left = 51
      Top = 14
      Width = 65
      Height = 13
      Caption = 'Procedimento'
      FocusControl = DBEdit2
    end
    object Label5: TLabel
      Left = 299
      Top = 14
      Width = 21
      Height = 13
      Caption = 'Tipo'
      FocusControl = DBEdit3
    end
    object DBEdit1: TDBEdit
      Left = 11
      Top = 30
      Width = 34
      Height = 21
      DataField = 'Codigo'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 51
      Top = 30
      Width = 244
      Height = 21
      DataField = 'Procedimento'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 299
      Top = 30
      Width = 124
      Height = 21
      DataField = 'Tipo'
      DataSource = DataSource1
      TabOrder = 2
    end
  end
  object DataSource1: TDataSource
    DataSet = dmWorkClinica.tbProcedimentos
    OnStateChange = DataSource1StateChange
    Left = 256
    Top = 161
  end
end
