unit unProcedimentos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Mask, DBCtrls, StdCtrls, Buttons, ExtCtrls;

type
  TForm1 = class(TForm)
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBNavigator1: TDBNavigator;
    edPesquisa: TEdit;
    cbOrdem: TComboBox;
    Panel3: TPanel;
    btIncluir: TBitBtn;
    btGravar: TBitBtn;
    btCancelar: TBitBtn;
    btExcluir: TBitBtn;
    Panel1: TPanel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    procedure DataSource1StateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses unDmWorkClinica;

{$R *.DFM}

procedure TForm1.DataSource1StateChange(Sender: TObject);
begin
 btIncluir.Enabled  := Se(dmWorkClinica.tbProcedimentos.State in [dsEdit,dsInsert], False, True);
 btGravar.Enabled   := Se(dmWorkClinica.tbProcedimentos.State in [dsEdit,dsInsert], True, False);
 btCancelar.Enabled := Se(dmWorkClinica.tbProcedimentos.State in [dsEdit,dsInsert], True, False);
 btExcluir.Enabled  := Se(dmWorkClinica.tbProcedimentos.State in [dsEdit,dsInsert], False, True);
end;

end.
