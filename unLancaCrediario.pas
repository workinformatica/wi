unit unLancaCrediario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, Mask, DBCtrls, Grids, DBGrids, DBTables;

type
  TfmLancaCrediario = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    btOk: TBitBtn;
    btCancelar: TBitBtn;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dbCodigo: TDBEdit;
    dsCprTmp: TDataSource;
    Label2: TLabel;
    dbValor: TDBEdit;
    Label3: TLabel;
    dbVencimento: TDBEdit;
    Label4: TLabel;
    dbNomeCliente: TDBEdit;
    dsClientes: TDataSource;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Panel5: TPanel;
    qrReceber: TQuery;
    dsReceber: TDataSource;
    DBGrid1: TDBGrid;
    qrReceberTIPO: TStringField;
    qrReceberPARCELA: TStringField;
    qrReceberCONTROLE: TStringField;
    qrReceberCLIENTE: TStringField;
    qrReceberCPFCGCCHQ: TStringField;
    qrReceberBANCOCHQ: TStringField;
    qrReceberNUMEROCHQ: TStringField;
    qrReceberDADOS: TMemoField;
    qrReceberVENDEDOR: TStringField;
    qrReceberEMISSAO: TDateField;
    qrReceberDOCUMENTO: TStringField;
    qrReceberVENCIMENTO: TDateField;
    qrReceberVALOR: TFloatField;
    qrReceberPAGAMENTO: TDateField;
    qrReceberDESCONTOS: TFloatField;
    qrReceberMULTA: TFloatField;
    qrReceberJUROS: TFloatField;
    qrReceberVALOR_PAGA: TFloatField;
    qrReceberVALOR_PAGO: TFloatField;
    qrReceberSITUACAO: TStringField;
    qrReceberRESTANTE: TBooleanField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLancaCrediario: TfmLancaCrediario;

implementation

uses unDmControle;

{$R *.DFM}

end.
