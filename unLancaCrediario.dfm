object fmLancaCrediario: TfmLancaCrediario
  Left = 203
  Top = 110
  Width = 544
  Height = 432
  BorderIcons = []
  Caption = 'Lan�ar Conv�nio'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 536
    Height = 41
    Align = alTop
    Caption = 'Conv�nio'
    Color = clHighlight
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 97
    Height = 323
    Align = alLeft
    TabOrder = 1
  end
  object Panel3: TPanel
    Left = 0
    Top = 364
    Width = 536
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object btOk: TBitBtn
      Left = 376
      Top = 4
      Width = 75
      Height = 33
      TabOrder = 0
      Kind = bkOK
    end
    object btCancelar: TBitBtn
      Left = 453
      Top = 4
      Width = 75
      Height = 33
      Caption = 'Cancelar'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object Panel4: TPanel
    Left = 97
    Top = 41
    Width = 439
    Height = 323
    Align = alClient
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 437
      Height = 104
      Align = alTop
      Caption = 'Dados do Cliente:'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 45
        Height = 13
        Caption = 'CLIENTE'
        FocusControl = dbCodigo
      end
      object Label2: TLabel
        Left = 8
        Top = 56
        Width = 42
        Height = 13
        Caption = 'VALOR1'
        FocusControl = dbValor
      end
      object Label3: TLabel
        Left = 80
        Top = 56
        Width = 35
        Height = 13
        Caption = 'DATA1'
        FocusControl = dbVencimento
      end
      object Label4: TLabel
        Left = 64
        Top = 16
        Width = 32
        Height = 13
        Caption = 'NOME'
        FocusControl = dbNomeCliente
      end
      object dbCodigo: TDBEdit
        Left = 8
        Top = 32
        Width = 41
        Height = 21
        DataField = 'CLIENTE'
        DataSource = dsCprTmp
        TabOrder = 0
      end
      object dbValor: TDBEdit
        Left = 8
        Top = 72
        Width = 64
        Height = 21
        DataField = 'VALOR1'
        DataSource = dsCprTmp
        TabOrder = 1
      end
      object dbVencimento: TDBEdit
        Left = 80
        Top = 72
        Width = 64
        Height = 21
        DataField = 'DATA1'
        DataSource = dsCprTmp
        TabOrder = 2
      end
      object dbNomeCliente: TDBEdit
        Left = 64
        Top = 32
        Width = 244
        Height = 21
        DataField = 'NOME'
        DataSource = dsClientes
        TabOrder = 3
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 105
      Width = 437
      Height = 105
      Align = alTop
      TabOrder = 1
      object Label5: TLabel
        Left = 8
        Top = 16
        Width = 60
        Height = 13
        Caption = 'ENDERECO'
        FocusControl = DBEdit5
      end
      object Label6: TLabel
        Left = 232
        Top = 16
        Width = 41
        Height = 13
        Caption = 'BAIRRO'
        FocusControl = DBEdit6
      end
      object Label7: TLabel
        Left = 8
        Top = 56
        Width = 40
        Height = 13
        Caption = 'CIDADE'
        FocusControl = DBEdit7
      end
      object Label8: TLabel
        Left = 168
        Top = 56
        Width = 14
        Height = 13
        Caption = 'UF'
        FocusControl = DBEdit8
      end
      object Label9: TLabel
        Left = 192
        Top = 56
        Width = 56
        Height = 13
        Caption = 'TELEFONE'
        FocusControl = DBEdit9
      end
      object Label10: TLabel
        Left = 296
        Top = 56
        Width = 49
        Height = 13
        Caption = 'CELULAR'
        FocusControl = DBEdit10
      end
      object DBEdit5: TDBEdit
        Left = 8
        Top = 32
        Width = 214
        Height = 21
        DataField = 'ENDERECO'
        DataSource = dsClientes
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 232
        Top = 32
        Width = 124
        Height = 21
        DataField = 'BAIRRO'
        DataSource = dsClientes
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 72
        Width = 154
        Height = 21
        DataField = 'CIDADE'
        DataSource = dsClientes
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 168
        Top = 72
        Width = 16
        Height = 21
        DataField = 'UF'
        DataSource = dsClientes
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 192
        Top = 72
        Width = 94
        Height = 21
        DataField = 'TELEFONE'
        DataSource = dsClientes
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 296
        Top = 72
        Width = 94
        Height = 21
        DataField = 'CELULAR'
        DataSource = dsClientes
        TabOrder = 5
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 210
      Width = 437
      Height = 112
      Align = alClient
      TabOrder = 2
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 435
        Height = 110
        Align = alClient
        DataSource = dsReceber
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CONTROLE'
            Title.Alignment = taCenter
            Title.Caption = 'Numero'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMISSAO'
            Title.Alignment = taCenter
            Title.Caption = 'Emiss�o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DOCUMENTO'
            Title.Alignment = taCenter
            Title.Caption = 'Documento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VENCIMENTO'
            Title.Alignment = taCenter
            Title.Caption = 'Vencimento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR'
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor'
            Visible = True
          end>
      end
    end
  end
  object dsCprTmp: TDataSource
    DataSet = dmControle.tbCprTmp
    Left = 226
    Top = 290
  end
  object dsClientes: TDataSource
    DataSet = dmControle.tbClientes
    Enabled = False
    Left = 258
    Top = 290
  end
  object qrReceber: TQuery
    DatabaseName = 'Controle'
    SessionName = 'seControle_1'
    SQL.Strings = (
      'Select * From Receber')
    Left = 226
    Top = 323
    object qrReceberTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'CONTROLE.Receber.TIPO'
      Size = 1
    end
    object qrReceberPARCELA: TStringField
      FieldName = 'PARCELA'
      Origin = 'CONTROLE.Receber.PARCELA'
      Size = 5
    end
    object qrReceberCONTROLE: TStringField
      FieldName = 'CONTROLE'
      Origin = 'CONTROLE.Receber.CONTROLE'
      Size = 14
    end
    object qrReceberCLIENTE: TStringField
      FieldName = 'CLIENTE'
      Origin = 'CONTROLE.Receber.CLIENTE'
      Size = 4
    end
    object qrReceberCPFCGCCHQ: TStringField
      FieldName = 'CPFCGCCHQ'
      Origin = 'CONTROLE.Receber.CPFCGCCHQ'
      Size = 18
    end
    object qrReceberBANCOCHQ: TStringField
      FieldName = 'BANCOCHQ'
      Origin = 'CONTROLE.Receber.BANCOCHQ'
      Size = 3
    end
    object qrReceberNUMEROCHQ: TStringField
      FieldName = 'NUMEROCHQ'
      Origin = 'CONTROLE.Receber.NUMEROCHQ'
      Size = 10
    end
    object qrReceberDADOS: TMemoField
      FieldName = 'DADOS'
      Origin = 'CONTROLE.Receber.DADOS'
      BlobType = ftMemo
      Size = 1
    end
    object qrReceberVENDEDOR: TStringField
      FieldName = 'VENDEDOR'
      Origin = 'CONTROLE.Receber.VENDEDOR'
      Size = 4
    end
    object qrReceberEMISSAO: TDateField
      FieldName = 'EMISSAO'
      Origin = 'CONTROLE.Receber.EMISSAO'
    end
    object qrReceberDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
      Origin = 'CONTROLE.Receber.DOCUMENTO'
      Size = 12
    end
    object qrReceberVENCIMENTO: TDateField
      FieldName = 'VENCIMENTO'
      Origin = 'CONTROLE.Receber.VENCIMENTO'
    end
    object qrReceberVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'CONTROLE.Receber.VALOR'
    end
    object qrReceberPAGAMENTO: TDateField
      FieldName = 'PAGAMENTO'
      Origin = 'CONTROLE.Receber.PAGAMENTO'
    end
    object qrReceberDESCONTOS: TFloatField
      FieldName = 'DESCONTOS'
      Origin = 'CONTROLE.Receber.DESCONTOS'
    end
    object qrReceberMULTA: TFloatField
      FieldName = 'MULTA'
      Origin = 'CONTROLE.Receber.MULTA'
    end
    object qrReceberJUROS: TFloatField
      FieldName = 'JUROS'
      Origin = 'CONTROLE.Receber.JUROS'
    end
    object qrReceberVALOR_PAGA: TFloatField
      FieldName = 'VALOR_PAGA'
      Origin = 'CONTROLE.Receber.VALOR_PAGA'
    end
    object qrReceberVALOR_PAGO: TFloatField
      FieldName = 'VALOR_PAGO'
      Origin = 'CONTROLE.Receber.VALOR_PAGO'
    end
    object qrReceberSITUACAO: TStringField
      FieldName = 'SITUACAO'
      Origin = 'CONTROLE.Receber.SITUACAO'
      Size = 1
    end
    object qrReceberRESTANTE: TBooleanField
      FieldName = 'RESTANTE'
      Origin = 'CONTROLE.Receber.RESTANTE'
    end
  end
  object dsReceber: TDataSource
    DataSet = qrReceber
    Left = 258
    Top = 323
  end
end
